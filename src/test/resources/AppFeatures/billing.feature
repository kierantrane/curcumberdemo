Feature: Calculate billing amount

Scenario Outline: billing amount
Given user is on billing page
When user enters billing amount "<billingamount>"
When user enters tax amount "<taxamount>"
And user enters calculate button
Then it gives the final amount "<finalamount>"
#Comment here

		Examples:
		|billingamount |taxamount |finalamount |
		|100 					 |10 				|110 				 |
		|500 					 |20 				|520 				 |
		|100 					 |5.5 			|105.5 			 |
		
		#Comment here too