Feature: Login Page
	In order to test login page
	As a registered user
	I want to specify the login conditions	
	
	#Then is validation

  Scenario: Amazon login page
    Given user is on the Amazon login page 
    And Sign in button is displayed correctly
		When the user select the Sign In button
		Then the sign in screen is displayed
		When the user enters "kieran@kieran.com" in the username field
		And the user enters "password" in the password field
		And the user select login button
		Then the homepage is displayed 
		And the title of the home page is "Amazon"
		But login button is not displayed

  