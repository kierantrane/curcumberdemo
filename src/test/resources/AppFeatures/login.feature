Feature: Login Feature

#Data driven approach with Scenario Outline
Scenario Outline: Login fail - possible combinations
Given user is on Application landing page
When a user clicks on Sign in button
Then user is displayed login screen
When user enters "<UserName>" in the username field
		And user enters "<Password>" in the password field
		And user clicks Sign In button
		Then user gets login failed error message
		
		Examples:
		| UserName 					| Password |
		| incorrectUserName | 123456   |
		| Kierantest 				| testpassIncorrect |
		| incorrect 				| incorrect |