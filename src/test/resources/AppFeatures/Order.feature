Feature: Amazon Order Page
  In order to check my order details
  As a registered user
  I want to specify the features of order page
  
 Background:
 Given a registered user exists
Given user is on the Amazon page 
When the user enters a username + password
When a user logs in
Then user navigates to the Order page
  
  
Scenario: Check previous order details
When user clicks on the Order link
Then the user clicks on the previous order details
  
 Scenario: Check open order details
When user clicks on the open Order link
Then the user clicks on the open order details

 Scenario: Check cancelled order details
When user clicks on the cancelled Order link
Then the user clicks on the cancelled order details