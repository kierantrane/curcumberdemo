@All
Feature: Uber Booking Feature
  
 @SmokeTest
  Scenario: Booking Cab
  Given User wants to select a car type "smoke_sedan" from uber app
  When user selects car "sedan" and pickup point "Glanmire" and drop location "Cork City"
  Then Driver starts the journey
  And Driver ends the journey
  Then User pays 30 Euro 
  
  @SmokeTest
   Scenario: Booking Cab
  Given User wants to select a car type "smoke_tesla" from uber app
  When user selects car "sedan" and pickup point "knockratta" and drop location "Cork City"
  Then Driver starts the journey
  And Driver ends the journey
  Then User pays 50 Euro 
  
  @Production
   Scenario: Booking Cab
  Given User wants to select a car type "prod_suv" from uber app
  When user selects car "sedan" and pickup point "galway" and drop location "Cork City"
  Then Driver starts the journey
  And Driver ends the journey
  Then User pays 300 Euro 
  
    #Given I want to write a step with precondition
    #And some other precondition
    #When I complete action
    #And some other action
    #And yet another action
    #Then I validate the outcomes
    #And check more outcomes

