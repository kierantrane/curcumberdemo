Feature: User Registration

Scenario: User registration with differnt data
Given User is on the registration page
When User enters firstname
  	| Kieran | Automation | kieran@kieran.com | 99999 | Cork |
    | Tom | manual  | tom@kieran.com | 33399 | Dublin |
    | Anna | HR  | anna@kieran.com | 22399 | Waterford  |
 Then user registration should be successful
 
 
 
 Scenario: User registration with differnt data with columns
 Given User is on the registration page
When User enters the following data details with columns
		| firstName | lastName | email | phoneNumber | city |
  	| Kieran | OSullivan   | kieran@kieran.com | +94353459999 | Cork |
    | Tom 	 | Barry       | tom@kieran.com | +333543599 | Dublin |
    | Anna   | LastName    | anna@kieran.com | +22456399 | Waterford  |
 Then user registration should be successful