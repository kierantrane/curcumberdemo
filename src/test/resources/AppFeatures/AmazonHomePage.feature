Feature: Home page
		In order to test the Amazon home page functionality
		As a registered user
		I want to specify the features of Home page	
		
		Background: 
		Given user is on the home page

		Scenario: Home page Top Panel Section
			Then the user gets a Amazon search field
			And username is displayed next to the search filed
			And Amazon log is displayed on the left
			And Cart dis displayed on the right side
			
			
			Scenario: Amazon Todays deals section
			When user selects the Todays deals on the top left
			Then Epic Daily Deals is displayed
			And user gets product image and price details
			And user can see more items by clickling on the carousel
			
			Scenario: Amazon footer section
			Then the user can select the region by country 
			| Australia |
			| Brazil |
			| France |
			And the user is displayed all the Amazon service links
			| Amazon Devices |
			| Shop with Points |
			| Amazon Music| 
			And the privacy policy links
			| Conditions of Use |
			| Privacy Notice | 
			| Interest-Based Ads |
			