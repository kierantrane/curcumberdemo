package MyHooks;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.Scenario;

public class AmazonHooks {

	
	@Before("@Smoke")
	public void setup_database(Scenario sc) {
		System.out.println("");
		System.out.println("Launch the setup_database");
		System.out.println("Scenario is = "+sc.getName());

	}
	
	
//	@Before(order = 2)
//	public void setup_database(Scenario sc) {
//		System.out.println("Launch the setup_database");
//		System.out.println("Scenario is = "+sc.getName());
//
//	}

	@Before(order = 1)
	public void setup_browser() {
		System.out.println("Launch the setup_browser");

	}

//	@After(order = 2)
//	public void tearDown_close(Scenario sc) {
//		System.out.println("Close the browser");
//		System.out.println("Scenario is = "+sc.getName());
//	}
	
	@After("@Smoke")
	public void tearDown_close(Scenario sc) {
		System.out.println("");
		System.out.println("Close the browser");
		System.out.println("Scenario is = "+sc.getName());
	}

//	@After(order = 1)
//	public void tearDown_logout() {
//		System.out.println("Logout of the application");
//	}
//	
//	@BeforeStep
//	public void takeScreenshot() {
//		System.out.println("Take a Screenshot");
//	}
//	
//	@AfterStep
//	public void refreshThePage() {
//		System.out.println("Refresh the page");
//	}

}
