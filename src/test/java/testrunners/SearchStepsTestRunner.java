package testrunners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/AppFeatures/Search.feature" }, 
		glue = {"stepdefinations", "MyHooks"},
		tags = "@Smoke or @Regression"
		//,
		//tags = "@All"
		//tags = "@SmokeTest or @Production"
		//plugin = {"pretty"}
		)


public class SearchStepsTestRunner {

}
