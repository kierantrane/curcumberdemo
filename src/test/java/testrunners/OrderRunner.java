package testrunners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

//Amazon one
@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/AppFeatures/Order.feature" }, 
		glue = {"stepdefinations"},
		plugin = {"pretty"},
		monochrome = true
		//publish = true
		)


public class OrderRunner {

}
