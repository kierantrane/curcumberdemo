package testrunners;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {"src/test/resources/AppFeatures/Uber.feature" }, 
		glue = {"stepdefinations", "MyHooks"},
		tags = "@All",
		plugin = {"json:target/MyReports/report_json.json",
				   "junit:target/MyReports/report_xml.xml"},
		monochrome = true
		//,dryRun = true  //This tells you when there is a step in the step definition missing
		//,publish = true  //No need as its added in by cucumber.properties
		//, "pretty"}
		
		//, plugin = {"pretty"}
		//tags = "@SmokeTest or @Production"
		)


public class UberTestRunner {

}
