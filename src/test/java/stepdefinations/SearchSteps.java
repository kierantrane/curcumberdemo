package stepdefinations;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
//import io.cucumber.messages.types.Product;
import AmazonImplementation.Product;
import AmazonImplementation.Search;

public class SearchSteps {
	
	Product product;
	Search search;
	
	@Given("I have a search field on Amazon Page")
	public void i_have_a_search_field_on_amazon_page() {
	 System.out.println("Step 1: I am on the search page ");
	}

	//@When("^I search for a product with \"([^\"]+)\"  and price (\\\\d+)$")  //{string} {int}
	@When("I search for a product with {string} and price {int}")
	public void i_search_for_a_product_with_and_price(String productName, Integer price) {
		System.out.println("Step 2: Search the product "+productName+" and price = "+price);
		
		product = new Product(productName, price);
	}

	@Then("Product with name {string} should be displayed")
	public void product_with_name_should_be_displayed(String productName) {
		 System.out.println("Step 3: Procuct name ="+productName+ " is displayed");
		 
		 search = new Search();
		String name =  search.displayProduct(product);
		 System.out.println("Product name is = "+name);
		 Assert.assertEquals(product.getProductName(), name);
	}
	
	@Then("Order id is {int} and userName is {string}")
	public void order_id_is_and_user_name_is(Integer orderId, String userName) {
		System.out.println("The order id ="+orderId+" and user nmae = "+userName); 
	}

}
