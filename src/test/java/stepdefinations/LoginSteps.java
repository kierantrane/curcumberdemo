package stepdefinations;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class LoginSteps {

	@Given("user is on Application landing page")
	public void user_is_on_application_landing_page() {
	    System.out.println("user is on Application landing page");
	}

	@When("a user clicks on Sign in button")
	public void a_user_clicks_on_sign_in_button() {
		System.out.println("a user clicks on Sign in button");
	}

	@Then("user is displayed login screen")
	public void user_is_displayed_login_screen() {
		System.out.println("user is displayed login screen");
	}

	@When("user enters {string} in the username field")
	public void user_enters_in_the_username_field(String string) {
		System.out.println("user enters {string} in the username field");
	}

	@When("user enters {string} in the password field")
	public void user_enters_in_the_password_field(String string) {
		System.out.println("user enters {string} in the password field");
	}

	@When("user clicks Sign In button")
	public void user_clicks_sign_in_button() {
		System.out.println("user clicks Sign In button");
	}

	@Then("user gets login failed error message")
	public void user_gets_login_failed_error_message() {
		System.out.println("user gets login failed error message");
	}
	
}
