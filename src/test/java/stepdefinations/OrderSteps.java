package stepdefinations;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
//import io.cucumber.messages.types.Product;

public class OrderSteps {

	@Given("a registered user exists")
	public void a_registered_user_exists() {

	}

	@Given("user is on the Amazon page")
	public void user_is_on_the_amazon_page() {

	}

	@When("the user enters a username + password")
	public void the_user_enters_a_username_password() {

	}

	@When("a user logs in")
	public void a_user_logs_in() {
		
	}

	@Then("user navigates to the Order page")
	public void user_navigates_to_the_order_page() {

	}

	@When("user clicks on the Order link")
	public void user_clicks_on_the_order_link() {

	}

	@Then("the user clicks on the previous order details")
	public void the_user_clicks_on_the_previous_order_details() {

	}

	@When("user clicks on the open Order link")
	public void user_clicks_on_the_open_order_link() {

	}

	@Then("the user clicks on the open order details")
	public void the_user_clicks_on_the_open_order_details() {

	}

	@When("user clicks on the cancelled Order link")
	public void user_clicks_on_the_cancelled_order_link() {

	}

	@Then("the user clicks on the cancelled order details")
	public void the_user_clicks_on_the_cancelled_order_details() {

	}

}
