package stepdefinations;

import java.util.List;
import java.util.Map;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class UserRegistrationSteps {

	
	
	@Given("User is on the registration page")
	public void user_is_on_the_registration_page() {
	    System.out.println("User navigated to the rego page");
	}

	@When("User enters firstname")
	public void user_enters_firstname(DataTable dataTable) {
		System.out.println("List <List<String>> - > User enters their first name + last name");
		
		List <List<String>> userList = dataTable.asLists(String.class);
		
		for(List<String> e : userList) {
			
			System.out.println("Displying record ="+e);
		}
	}
	
	@When("User enters the following data details with columns")
	public void user_enters_the_following_data_details_with_columns(DataTable dataTable) {
		System.out.println("List <Map<String, String>> -> User enters the following data details with columns");
		List <Map<String, String>> userList = dataTable.asMaps(String.class, String.class);
		
		//System.out.println("User list = "+userList);
		//System.out.println("User first in list = "+userList.get(0).get("firstName"));
		for(Map<String, String> e : userList) {
			System.out.println("");
			System.out.println("Print out First name in User list = "+e.get("firstName"));
			System.out.println("Print out lastName in User list = "+e.get("lastName"));
			System.out.println("Print out email in User list = "+e.get("email"));
			System.out.println("Print out phoneNumber in User list = "+e.get("phoneNumber"));
			System.out.println("Print out city in User list = "+e.get("city"));
			System.out.println("");
		}
		
	}

	@Then("user registration should be successful")
	public void user_registration_should_be_successful() {
		System.out.println("Registration should be successful");
	  
	}
	

	
}
