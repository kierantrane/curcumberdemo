package stepdefinations;

import java.util.List;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.datatable.DataTable;

public class AmazonHomePage {
	
	@Given("user is on the home page")
	public void user_is_on_the_home_page() {
	    
	}

	@Then("the user gets a Amazon search field")
	public void the_user_gets_a_amazon_search_field() {
	   
	}

	@Then("username is displayed next to the search filed")
	public void username_is_displayed_next_to_the_search_filed() {
	   
	}

	@Then("Amazon log is displayed on the left")
	public void amazon_log_is_displayed_on_the_left() {
	    
	}

	@Then("Cart dis displayed on the right side")
	public void cart_dis_displayed_on_the_right_side() {
	    
	}

	@When("user selects the Todays deals on the top left")
	public void user_selects_the_todays_deals_on_the_top_left() {
	    
	}

	@Then("Epic Daily Deals is displayed")
	public void epic_daily_deals_is_displayed() {
	   
	}

	@Then("user gets product image and price details")
	public void user_gets_product_image_and_price_details() {
	   
	}

	@Then("user can see more items by clickling on the carousel")
	public void user_can_see_more_items_by_clickling_on_the_carousel() {
	   
	}

	@Then("the user can select the region by country")
	public void the_user_can_select_the_region_by_country(DataTable dataTable) {
	    
		List<List<String>> countryList = (List) dataTable.asList();
		System.out.println(countryList);
	  
	}

	@Then("the user is displayed all the Amazon service links")
	public void the_user_is_displayed_all_the_amazon_service_links(DataTable dataTable) {
		List<List<String>> amazonServices = (List) dataTable.asList();
		System.out.println(amazonServices);
	}

	@Then("the privacy policy links")
	public void the_privacy_policy_links(DataTable dataTable) {
		List<List<String>> privacypolicy = (List) dataTable.asList();
		System.out.println(privacypolicy);
	   
	}

}
