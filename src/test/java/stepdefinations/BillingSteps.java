package stepdefinations;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import javax.annotation.processing.SupportedSourceVersion;

import org.junit.Assert;

public class BillingSteps {

	double billingAmount;
	double taxAmount;
	double finalAmount;

	@Given("user is on billing page")
	public void user_is_on_billing_page() {
		System.out.println("@Given(\"user is on billing page\")");

	}

	@When("user enters billing amount {string}")
	public void user_enters_billing_amount(String billingAmount) {
		this.billingAmount = Double.parseDouble(billingAmount);
		System.out.println("@When(\"user enters billing amount {string}\")");
	}

	@When("user enters tax amount {string}")
	public void user_enters_tax_amount(String taxAmount) {
		this.taxAmount = Double.parseDouble(taxAmount);
		System.out.println("@When(\"user enters tax amount {string}\")");
	}

	@When("user enters calculate button")
	public void user_enters_calculate_button() {
		System.out.println("user_enters_calculate_button");
		System.out.println("@When(\"user enters calculate button\")");
		
	}

	@Then("it gives the final amount {string}") 
	public void it_gives_the_final_amount(String expectedFinalAmount) {
		this.finalAmount = this.billingAmount + this.taxAmount;
		System.out.println("Expected final amount "+Double.parseDouble(expectedFinalAmount));
		System.out.println("this.finalAmount = "+this.finalAmount);
		Assert.assertTrue(this.finalAmount == Double.parseDouble(expectedFinalAmount));
		
		System.out.println("@Then(\"it gives the final amount {string}\") ");
	}
}
