package stepdefinations;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class AmazonLogin {

	@Given("user is on the Amazon login page")
	public void user_is_on_the_amazon_login_page() {
		System.out.println("@Given user is on the Amazon login page");
	   
	}

	@Given("Sign in button is displayed correctly")
	public void sign_in_button_is_displayed_correctly() {
	    System.out.println("@Given Sign in button is displayed correctly");
	    
	}

	@When("the user select the Sign In button")
	public void the_user_select_the_sign_in_button() {
		System.out.println("@When the user select the Sign In button" );
	}

	@Then("the sign in screen is displayed")
	public void the_sign_in_screen_is_displayed() {
		System.out.println("the user select the Sign In button");
	}

	@When("the user enters {string} in the username field")
	public void the_user_enters_in_the_username_field(String string) {
		System.out.println("@Then the user enters {string} in the username field");
	}

	@When("the user enters {string} in the password field")
	public void the_user_enters_in_the_password_field(String string) {
		System.out.println("@When the user enters {string} in the password field");
	}

	@When("the user select login button")
	public void the_user_select_login_button() {
		System.out.println("@When the user select login button");
	}

	@Then("the homepage is displayed")
	public void the_homepage_is_displayed() {
		System.out.println("@Then the homepage is displayed");
	}

	@Then("the title of the home page is {string}")
	public void the_title_of_the_home_page_is(String string) {
		System.out.println("@Then the title of the home page is {string}");
	}

	@Then("login button is not displayed")
	public void login_button_is_not_displayed() {
		System.out.println("@Then login button is not displayed");
	}
	
}
