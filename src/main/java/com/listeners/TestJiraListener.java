//package com.listeners;
//
//import org.codehaus.plexus.util.ExceptionUtils;
//import org.testng.ITestContext;
//import org.testng.ITestListener;
//import org.testng.ITestResult;
//
//import com.qa.util.JiraPolicy;
//import com.qa.util.JiraServiceProvider;
//
//public class TestJiraListener implements ITestListener {
//
//	@Override
//	public void onTestStart(ITestResult result) {
//		System.out.println("onTestStart ="+result);
//
//	}
//
//	@Override
//	public void onTestSuccess(ITestResult result) {
//		System.out.println("onTestSuccess ="+result);
//
//	}
//
//	@Override
//	public void onTestFailure(ITestResult result) {
//		JiraPolicy jiraPolicy = result.getMethod().getConstructorOrMethod().getMethod().getAnnotation(JiraPolicy.class);
//		boolean isTicketReady = jiraPolicy.logTicketReady();
//		if (isTicketReady) {
//			String url = "https://snipp-interactive.atlassian.net/";
//			String user = "kieran.ocallaghan@snipp.com";
//			
//			System.out.println("isTicketReady ="+isTicketReady);
//
//			System.out.println("Is ticket ready for JIRA" + isTicketReady);
//			
//			JiraServiceProvider jirsSP = new JiraServiceProvider(url, user, "6Fhk3TxLt3MxsMAwjU3kDA7C", "TOP");
//			String issueSummary = result.getMethod().getConstructorOrMethod().getMethod().getName()
//					+ " got fiailed due to some assertion or exception";
//			String issueDescription = result.getThrowable().getMessage() + "\n";
//
//			issueDescription.concat(ExceptionUtils.getFullStackTrace(result.getThrowable()));
//			jirsSP.createJiraTicket("Bug", issueSummary, issueDescription, "Kieran O'Callaghan");
//		}
//		else if (!isTicketReady){
//			System.out.println("Ticket is not ready ");
//		}
//	}
//
//	@Override
//	public void onTestSkipped(ITestResult result) {
//		System.out.println("onTestSkipped ="+result);
//
//	}
//
//	@Override
//	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
//		System.out.println("onTestFailedButWithinSuccessPercentage ="+result);
//
//	}
//
//	@Override
//	public void onStart(ITestContext context) {
//		System.out.println("onStart ="+context.toString());
//
//	}
//
//	@Override
//	public void onFinish(ITestContext context) {
//		System.out.println("onFinish ="+context.toString());
//
//	}
//
//}
